package com.talixa.specan.demod.fsk;

public class BaudotDecoder {

	private static final char[] LETTERS = {'*','E','\n','A',' ','S'  ,'I','U','\r','D','R','J'  ,'N','F','C','K','T','Z','L','W','H','Y','P','Q','O','B','G','*','M','X','V','*'};
	private static final char[] US_NUM  = {'*','3','\n','-',' ','\07','8','7','\r','$','4','\'' ,',','!',':','(','5','"',')','2','#','6','0','1','9','?','&','*','.','/',';','*'};
	private static final char[] ITU_NUM = {'*','3','\n','-',' ','\'' ,'8','7','\r','*','4','\07',',','!',':','(','5','+',')','2','L','6','0','1','9','?','&','*','.','/','=','*'};
	public enum Mode {US, CCITT2};
	
	private byte[] data;
	private Mode mode;
	
	public BaudotDecoder(byte[] data, Mode mode) {
		this.data = data;
		this.mode = mode;		
	}

	public String decode() {		
		boolean figures = false;
		
		StringBuilder baudot = new StringBuilder();
		// read data in 5 byte chunks (8 baudot chars)
		for(int i = 0; i < data.length; ++i) {		
			int value = data[i];
			if (value == 27) {
				figures = true;
			} else if (value == 31) {
				figures = false;
			} else {
				if (!figures) {
					baudot.append(LETTERS[value]);
				} else if (mode.equals(Mode.US)) {
					baudot.append(US_NUM[value]);
				} else {
					baudot.append(ITU_NUM[value]);
				}
			}
		}
		
		return baudot.toString();					
	}			
}
