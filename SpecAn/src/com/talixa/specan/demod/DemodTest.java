package com.talixa.specan.demod;

import com.talixa.specan.demod.dtmf.DTMFDecoder;
import com.talixa.specan.demod.fsk.RttyDemod;
import com.talixa.specan.demod.ook.CwDemod;
import com.talixa.specan.demod.psk.Psk31Demod;

public class DemodTest {
 
	/*
	 * Command line client to demodulate data
	 */
	public static void main(String args[]) {		
		Psk31Demod.testDemod();
		RttyDemod.testDemod();
		CwDemod.testDemod();
		DTMFDecoder.testDemod();
	}
}
