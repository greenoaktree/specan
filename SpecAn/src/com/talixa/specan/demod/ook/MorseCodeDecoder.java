package com.talixa.specan.demod.ook;

public class MorseCodeDecoder {

	private static String[] MORSE_CHARS = new String[] {
		".-", "-...", "-.-.", "-..", ".", "..-.", "--.",
		"....", "..", ".---", "-.-", ".-..", "--", "-.",
		"---", ".--.", "--.-", ".-.", "...", "-", "..-",
		"...-", ".--", "-..-", "-.--","--.."
	};
	
	private static String[] MORSE_NUMS = new String[] {
		"-----", ".----", "..---", "...--", "....-",
		".....", "-....", "--...", "---..", "----."
	};
	
	public static String decodeMorse(String input) {
		StringBuffer msg = new StringBuffer();
		String[] words = input.split("\n");
		for(String word : words) {
			String[] chars = word.split(" ");
			for(String ch : chars) {
				if (ch.length() == 5) {
					for(int i = 0; i < MORSE_NUMS.length; ++i) {
						if (ch.equals(MORSE_NUMS[i])) {
							msg.append(i);
						}
					}
				} else {
					for(int i = 0; i < MORSE_CHARS.length; ++i) {
						if (ch.equals(MORSE_CHARS[i])) {
							msg.append((char)(i+97));
						}
					}					
				}
			}
			msg.append(" ");
		}
		return msg.toString();
	}
}
