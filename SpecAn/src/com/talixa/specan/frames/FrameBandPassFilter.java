package com.talixa.specan.frames;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.talixa.audio.wav.WaveFile;
import com.talixa.specan.SpectrumAnalyzer;
import com.talixa.specan.dsp.BandPassFilter;
import com.talixa.specan.dsp.SharedDSPFunctions;
import com.talixa.specan.shared.SpecAnConstants;

public class FrameBandPassFilter {

	private static final int ROW_COUNT = 3;
	private static final int WIDTH = SpecAnConstants.DEFAULT_FRAME_WIDTH;
	private static final int HEIGHT = SpecAnConstants.HEIGHT_PER_PANEL * (ROW_COUNT+1);
	
	public static void createAndShowGUI(final String inputFile) {
		// Create frame
		final JFrame frame = new JFrame(SpecAnConstants.TITLE_FILTER);
		frame.setLayout(new BorderLayout());
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			
		// panel outputfile
		JPanel outputDataPanel = new JPanel(new FlowLayout());
		final JLabel outputFileLabel = new JLabel(SpecAnConstants.NO_FILE_SELECTED);
		JButton selectOutputButton = new JButton(SpecAnConstants.SELECT_OUTPUT_FILE);
		selectOutputButton.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser fc = new JFileChooser();
				int result = fc.showSaveDialog(frame);
				if (result == JFileChooser.APPROVE_OPTION) {
					outputFileLabel.setText(fc.getSelectedFile().getAbsolutePath());		
				}					
			}
		});						
		outputDataPanel.add(selectOutputButton);
		outputDataPanel.add(outputFileLabel);
		
		// low freq panel
		JPanel lowFreqPanel = new JPanel(new FlowLayout());
		lowFreqPanel.add(new JLabel("Low Freq"));
		final JTextField lowFreqText = new JTextField(10);
		lowFreqPanel.add(lowFreqText);
		
		// high freq panel
		JPanel highFreqPanel = new JPanel(new FlowLayout());
		highFreqPanel.add(new JLabel("High Freq"));
		final JTextField highFreqText = new JTextField(10);
		highFreqPanel.add(highFreqText);
			
		// put panels together
		JPanel mainPanel = new JPanel(new GridLayout(ROW_COUNT, 1));
		mainPanel.add(outputDataPanel);
		mainPanel.add(lowFreqPanel);
		mainPanel.add(highFreqPanel);
		
		JButton okButton = new JButton(SpecAnConstants.LABEL_OK);
		okButton.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				int low = Integer.valueOf(lowFreqText.getText());
				int high = Integer.valueOf(highFreqText.getText());				
				
				WaveFile wave1 = SpectrumAnalyzer.readWaveFile(inputFile);		
				if (wave1 != null) {
					short[] inputData = SharedDSPFunctions.extractWaveFileData(wave1);					
					try {
						BandPassFilter.filterData(inputData, low, high, outputFileLabel.getText());
					} catch (IOException e1) {
						JOptionPane.showMessageDialog(frame, SpecAnConstants.ERROR_FILE_LOAD);
					}
				}								
												
				frame.dispose();
			}
		});
				
		// Add to frame
		frame.add(mainPanel, BorderLayout.CENTER);
		frame.add(okButton, BorderLayout.SOUTH);
				
		// Set location and display
		Dimension screenSize = new Dimension(Toolkit.getDefaultToolkit().getScreenSize());
		frame.setPreferredSize(new Dimension(WIDTH,HEIGHT));
		int left = (screenSize.width/2) - (WIDTH/2);
		int top  = (screenSize.height/2) - (HEIGHT/2);
		frame.pack();
		frame.setLocation(left,top);
		frame.setVisible(true);						
	}		
}
