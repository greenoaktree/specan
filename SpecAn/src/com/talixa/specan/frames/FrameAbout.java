package com.talixa.specan.frames;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

import com.talixa.specan.listeners.ExitActionListener;
import com.talixa.specan.shared.SpecAnConstants;

public class FrameAbout {
	
	private static final int WIDTH = 440;
	private static final int HEIGHT = 250;
	
	private static final String ABOUT_CONTENTS = 
			"<html><center>A simple spectrum analyzer for audio files. Includes spectrum analyzer, waterfall, " +
			"and oscilloscope display. Currently works with 16-bit PCM files with an 8K sample rate. " +
		    "<br><br>Other functions include tone generator, audio mixer, bandpass filters, and demodulators " +
			"for PSK31 and RTTY. Demod functions are experimental. " +
			"<br><br>Version: " + SpecAnConstants.VERSION + 
			"<br>Copyright 2014, Talixa Software</center></html>";
	
	public static void createAndShowGUI() {
		// Create frame
		JFrame frame = new JFrame(SpecAnConstants.TITLE_ABOUT);
		frame.setLayout(new BorderLayout());
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	
		// create components
		JLabel text = new JLabel(ABOUT_CONTENTS);	
		JButton ok = new JButton(SpecAnConstants.LABEL_OK);
		ok.addActionListener(new ExitActionListener(frame));
				
		// Add to frame
		frame.add(text, BorderLayout.CENTER);
		frame.add(ok, BorderLayout.SOUTH);
				
		// Set location and display
		Dimension screenSize = new Dimension(Toolkit.getDefaultToolkit().getScreenSize());
		frame.setPreferredSize(new Dimension(WIDTH,HEIGHT));
		int left = (screenSize.width/2) - (WIDTH/2);
		int top  = (screenSize.height/2) - (HEIGHT/2);
		frame.pack();
		frame.setLocation(left,top);
		frame.setVisible(true);						
	}	
}
