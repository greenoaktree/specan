package com.talixa.specan.frames;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.talixa.specan.dsp.ToneGenerator;
import com.talixa.specan.shared.SpecAnConstants;

public class FrameToneGenerator {

	private static final int ROW_COUNT = 5;
	private static final int WIDTH = SpecAnConstants.DEFAULT_FRAME_WIDTH;
	private static final int HEIGHT = SpecAnConstants.HEIGHT_PER_PANEL * (ROW_COUNT+1);
		
	public static void createAndShowGUI() {
		// Create frame
		final JFrame frame = new JFrame(SpecAnConstants.TITLE_TONE_GENERATOR);
		frame.setLayout(new BorderLayout());
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			
		// panel outputfile
		JPanel outputDataPanel = new JPanel(new FlowLayout());
		final JLabel outputFileLabel = new JLabel(SpecAnConstants.NO_FILE_SELECTED);
		JButton selectOutputButton = new JButton(SpecAnConstants.SELECT_OUTPUT_FILE);
		selectOutputButton.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser fc = new JFileChooser();
				int result = fc.showSaveDialog(frame);
				if (result == JFileChooser.APPROVE_OPTION) {
					outputFileLabel.setText(fc.getSelectedFile().getAbsolutePath());		
				}					
			}
		});						
		outputDataPanel.add(selectOutputButton);
		outputDataPanel.add(outputFileLabel);
		
		// freq panel
		JPanel freqPanel = new JPanel(new FlowLayout());
		freqPanel.add(new JLabel("Frequency"));
		final JTextField freqText = new JTextField(10);
		freqPanel.add(freqText);
		
		// volume panel
		JPanel volPanel = new JPanel(new FlowLayout());
		volPanel.add(new JLabel("Volume"));
		final JTextField volText = new JTextField(10);
		volText.setText(".1");
		volPanel.add(volText);
		
		// volume panel
		JPanel lenPanel = new JPanel(new FlowLayout());
		lenPanel.add(new JLabel("Length"));
		final JTextField lenText = new JTextField(10);
		lenText.setText("60");
		lenPanel.add(lenText);
		
		// phase panel
		JPanel phasePanel = new JPanel(new FlowLayout());
		phasePanel.add(new JLabel("Phase"));
		final JTextField phaseText = new JTextField(10);
		phaseText.setText("0");
		phasePanel.add(phaseText);		
		
		// put panels together
		JPanel mainPanel = new JPanel(new GridLayout(ROW_COUNT, 1));
		mainPanel.add(outputDataPanel);
		mainPanel.add(freqPanel);
		mainPanel.add(volPanel);
		mainPanel.add(lenPanel);
		mainPanel.add(phasePanel);
		
		JButton okButton = new JButton(SpecAnConstants.LABEL_OK);
		okButton.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				int freq = Integer.valueOf(freqText.getText());
				double vol = Double.valueOf(volText.getText());
				int len = Integer.valueOf(lenText.getText());
				int phase = Integer.valueOf(phaseText.getText());
				
				if (!outputFileLabel.getText().equals(SpecAnConstants.NO_FILE_SELECTED)) {
					try {						
						ToneGenerator.generateWave(freq, vol, len, phase, outputFileLabel.getText());						
					} catch (IOException e1) {						
						JOptionPane.showMessageDialog(frame, SpecAnConstants.ERROR_BAD_WAVE);
					}					
					frame.dispose();
				} 																
			}
		});
				
		// Add to frame
		frame.add(mainPanel, BorderLayout.CENTER);
		frame.add(okButton, BorderLayout.SOUTH);
				
		// Set location and display
		Dimension screenSize = new Dimension(Toolkit.getDefaultToolkit().getScreenSize());
		frame.setPreferredSize(new Dimension(WIDTH,HEIGHT));
		int left = (screenSize.width/2) - (WIDTH/2);
		int top  = (screenSize.height/2) - (HEIGHT/2);
		frame.pack();
		frame.setLocation(left,top);
		frame.setVisible(true);						
	}		
}
