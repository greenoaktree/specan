package com.talixa.specan.input;

import java.util.ArrayDeque;
import java.util.Queue;

import javax.sound.sampled.TargetDataLine;

import com.talixa.audio.utils.endian.EndianConverter;

public class RecordingThread extends Thread {
	
	private TargetDataLine line;
	private boolean done = false;
	private int frameSize;
	
	private Queue<Object> dataQueue = new ArrayDeque<Object>();
	
	public RecordingThread(TargetDataLine line, int frameSize) {
		this.line = line;
		this.frameSize = frameSize;
	}
	
	public void stopRecording() {
		this.done = true;
	}
	
	public synchronized short[] getNextFrame() {
		return (short[])dataQueue.poll();
	}
	
	public boolean frameAvailable() {
		return dataQueue.size() > 0;
	}
	
	public void run() {							    
		//byte[] data = new byte[line.getBufferSize() / 5];
		byte[] data = new byte[frameSize*2];		

		// Begin audio capture.
		line.start();

		while(!done) {
		   line.read(data, 0, data.length);		   
			short[] dataAsShort = new short[frameSize];
			for(int i = 0; i < frameSize; ++i) {
				dataAsShort[i] = (short)EndianConverter.littleEndianShortToJavaInt(data[i*2+1], data[i*2]);
			}
			synchronized(this) {
			   dataQueue.add(dataAsShort);
		   }
		}     															
	}
}
