package com.talixa.specan.input;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.TargetDataLine;

public class LineInputReader {
	
	private static final int SAMPLE_RATE = 8000;
	private static final int BITS_PER_SAMPLE = 16;
	private static AudioFormat format = new AudioFormat(SAMPLE_RATE, BITS_PER_SAMPLE, 1, true, false);
	
	private static TargetDataLine line = null;
	private RecordingThread recordingThread;
	private int frameSize;
	
	public LineInputReader(int frameSize) throws LineUnavailableException {
		DataLine.Info info = new DataLine.Info(TargetDataLine.class, format);
		if (!AudioSystem.isLineSupported(info)) {
			throw new LineUnavailableException("No input device");
		} else {
			if (line == null) {
				line = (TargetDataLine) AudioSystem.getLine(info);			
				line.open(format);
			}
		}
		this.frameSize = frameSize;
	}

	public short[] getNextFrame() throws InterruptedException {
		while (!recordingThread.frameAvailable()) {		
			Thread.sleep(50);
		}
		return recordingThread.getNextFrame();
	}
	
	public void startRecording() {	
		line.flush();
		recordingThread = new RecordingThread(line, frameSize);
		recordingThread.start();				
	}
	
	public void stopRecording() {
		recordingThread.stopRecording();
		recordingThread = null;
	}
}
