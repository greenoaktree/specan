package com.talixa.specan.input;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.TargetDataLine;

import com.talixa.audio.wav.WaveFile;
import com.talixa.audio.wav.WaveGenerator;

public class LineInputRecorder {
	
	private static final int SAMPLE_RATE = 8000;
	private static final int BITS_PER_SAMPLE = 16;
	private static final int BYTES_PER_SECOND = SAMPLE_RATE * (BITS_PER_SAMPLE / 8);

	private static AudioFormat format = new AudioFormat(SAMPLE_RATE, BITS_PER_SAMPLE, 1, true, false);	
	
	public static void record(String outputFile, int length) {			
		long bytesRequired = length * BYTES_PER_SECOND;
		
		DataLine.Info info = new DataLine.Info(TargetDataLine.class, format);		
		if (AudioSystem.isLineSupported(info)) {
			// Obtain and open the line.		
			try {
				TargetDataLine line = (TargetDataLine) AudioSystem.getLine(info);
			    line.open(format);
			    
			    // Assume that the TargetDataLine, line, has already
				// been obtained and opened.
				ByteArrayOutputStream out  = new ByteArrayOutputStream();
				int numBytesRead;
				byte[] data = new byte[line.getBufferSize() / 5];

				// Begin audio capture.
				line.start();

				// Here, stopped is a global boolean set by another thread.
				while(out.size() < bytesRequired) {
				   // Read the next chunk of data from the TargetDataLine.
				   numBytesRead =  line.read(data, 0, data.length);
				   // Save this chunk of data.
				   out.write(data, 0, numBytesRead);
				}     
				
				WaveFile test = WaveGenerator.generateWaveFromRaw16bitPcm(out.toByteArray());
				test.outputToFile(outputFile);
			
			} catch (LineUnavailableException e) {
				e.printStackTrace(); 
			} catch (IOException e) {
				e.printStackTrace();
			}	 
		} else {
			System.out.println("Format not supported");
		}
	}
	
	public static void main(String args[]) {		
		record("/home/thomas/test.wav", 10);			
	}
}
