package com.talixa.specan.listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

public class ExitActionListener implements ActionListener {
	
	private JFrame frame;
	public ExitActionListener(JFrame frame) {
		this.frame = frame;
	}
	
	public void actionPerformed(ActionEvent e) {	
		frame.dispose();
	}	
}
